﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGameController : MonoBehaviour {

	//Variable that store the raw game object to be instantiated;
	[SerializeField]
	private GameObject rawCard;

	//Array with cards images
	[SerializeField]
	private Sprite[] cardImgs;

	//Variable to store the 13 random cards generated
	string[,] storeCardsInfo = new string[13, 13];

	//Variable to append all 13 generated and configured gameObjects
	List<GameObject> allCardsToReturn = new List<GameObject>();

	//Variable to use in TryParse function
	private int n;

	private int arrayController = 0;

	//Populate the storeCardsInfo array with the randomed card names generated
	private void GenerateCardName(){
		storeCardsInfo = RandomCards.Randomize (13);
	}

	//This function return the right sprite based on the number and the nipe gived
	private Sprite GetSprite(string number, string nipe){
		int internNumber = 0;
		int internNipe = 0;

		// Treating letters
		switch (number) {
		case "A":
			internNumber = 0;
			break;
		case "Valete":
			internNumber = 10;
			break;
		case "Dama":
			internNumber = 11;
			break;
		case "Rei":
			internNumber = 12;
			break;
		default:
			internNumber = int.Parse (number) - 1;
			break;
		}

		//Positioning the nipes related to the position in array
		switch (nipe) {
		case "Paus":
			internNipe = 0;
			break;
		case "Ouro":
			internNipe = 1;
			break;
		case "Copas":
			internNipe = 2;
			break;
		case "Espadas":
			internNipe = 3;
			break;
		default:
			Debug.Log("Problema no retorno do nipe correta!!!");
			break;
		}

		return cardImgs[(internNumber * 4) + internNipe];
	}

	//This function instantiate the card and append to the list applying to the given parent
	public List<GameObject> GenerateCardAndParentIt(Transform parent){
		GameObject tempStoreCard = null;
		allCardsToReturn = new List<GameObject>();
		GenerateCardName ();
		//Instantiate, configure and parent the cards
		for (int i = 0; i < storeCardsInfo.GetLength (0); i++) {
			if (allCardsToReturn.Count >= 1) {
				tempStoreCard = Instantiate (rawCard, (allCardsToReturn[allCardsToReturn.Count - 1].transform.localPosition + new Vector3(0.6f, 0f, allCardsToReturn.Count)), Quaternion.identity);
			} else {
				tempStoreCard = Instantiate (rawCard, Vector3.zero, Quaternion.identity);
			}
			tempStoreCard.transform.SetParent (parent, false);
			tempStoreCard.GetComponent<SpriteRenderer> ().sprite = GetSprite (storeCardsInfo [i, 0], storeCardsInfo [i, 1]);
			tempStoreCard.name = storeCardsInfo [i, 0] + " de " + storeCardsInfo [i, 1];

			allCardsToReturn.Add (tempStoreCard);
		}
		//Organize the card in the player "hands"
		foreach (GameObject card in allCardsToReturn) {
			arrayController++;
			card.tag = "card";
			card.transform.localPosition = new Vector3 (card.transform.localPosition.x - (0.6f * (allCardsToReturn.Count/2)), 0f, (-0.1f * (float)arrayController));
		}

		arrayController = 0;

		return allCardsToReturn;
	}
}