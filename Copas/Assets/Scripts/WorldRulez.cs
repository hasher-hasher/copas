﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WorldRulez : MonoBehaviour {

	//GameController instance
	private GameController gc;

	//Variable that store the nipe of the current round
	private string nipeOfTheRound;

	private bool canPlayCopas;

	private Coroutine coroutine1;
	private Coroutine coroutine2;
	private Coroutine coroutine3;
	private Coroutine coroutine4;

	[SerializeField]
	private Text p1Points;
	[SerializeField]
	private Text p2Points;
	[SerializeField]
	private Text p3Points;
	[SerializeField]
	private Text p4Points;

	private int totalPoints;

	[SerializeField]
	private Text whoWin;

	void Start(){
		gc = GameObject.Find ("GameController").GetComponent<GameController> ();
		canPlayCopas = false;
		totalPoints = 0;
	}

	//Function that check the nipe of the current round
	public void NipeOfTheRound(){
		if (gc.allCenters [0].transform.childCount >= 1) {
			nipeOfTheRound = gc.allCenters [0].transform.GetChild (0).name.Split (' ').Last ();
			Debug.Log ("Nipe Permitido: " + nipeOfTheRound);
		} else {
			nipeOfTheRound = "none";
			Debug.Log ("Nipe Permitido: First nipe");
		}
	}

	//Function that check if the selected card can be played or not
	public bool CanIPlayThisCard(string nameOfTheCard, GameObject gameObjectThatIsCalling){
		Debug.Log ("Carta clicada: " + nameOfTheCard);
		//Generate or set the nipe of the round
		NipeOfTheRound ();
		//Temporary variable that store the fact that the player don't have the nipe of the round
		bool doesntHaveNipe = false;
		//Check permission to play the card
		/*if(nameOfTheCard.Split (' ').Last () == "Copas"){
			if (canPlayCopas && doesntHaveNipe) {
				Debug.Log ("Pode jogar Copas");
				return true;
			} else {
				Debug.Log ("Não pode jogar Copas");
				return false;
			}
		} else*/

		if(nipeOfTheRound != "none"){
			if (nipeOfTheRound == nameOfTheCard.Split (' ').Last ()) {
				Debug.Log ("Blow");
				return true;
			} else {
				for (int i = 0; i < gameObjectThatIsCalling.transform.parent.childCount; i++) {
					if (nipeOfTheRound == gameObjectThatIsCalling.transform.parent.GetChild (i).name.Split (' ').Last ()) {
						Debug.Log ("Existe carta do nipe do round");
						doesntHaveNipe = false;
						break;
					} else {
						doesntHaveNipe = true;
						if (nameOfTheCard.Split (' ').Last () == "Copas") {
							canPlayCopas = true;
						}
					}
				}
			}

			if (doesntHaveNipe) {
				Debug.Log ("Não tem nipe");
				//canPlayCopas = true;
				return true;
			} else {
				Debug.Log ("Tem nipe");
				return false;
			}
			return true;
		} else if(nipeOfTheRound == "none"){
			if (nameOfTheCard.Split (' ').Last () == "Copas" && canPlayCopas) {
				Debug.Log ("Jogando Primira Carta");
				return true;

			} else if (nameOfTheCard.Split (' ').Last () == "Copas" && !canPlayCopas) {
				return false;
			} else {
				Debug.Log ("Jogando Primira Carta");
				return true;
			}
			return true;
		}
		return true;	
	}

	public void WhoWinsAndCleanBoard(){
		RightNipe ();
	}

	private int HigherNumber(){
		string str;
		int higherNumber = 0;
		int[] allNumbers = new int[4];

		for (int i = 0; i < gc.allCenters.Count; i++) {
			str = gc.allCenters [i].transform.GetChild(0).name;
			switch (str.Split (' ')[0]) {
			case "A":
				allNumbers[i] = 14;
				break;
			case "Valete":
				allNumbers[i] = 11;
				break;
			case "Dama":
				allNumbers[i] = 12;
				break;
			case "Rei":
				allNumbers[i] = 13;
				break;
			default:
				allNumbers[i] = int.Parse(str.Split (' ')[0]);
				break;
			}
		}
		return Mathf.Max(allNumbers);
	}

	private void RightNipe(){
		int higherNumberInt = HigherNumber ();
		string higherNumberString = "";

		switch (higherNumberInt) {
		case 14:
			higherNumberString = "A";
			break;
		case 11:
			higherNumberString = "Valete";
			break;
		case 12:
			higherNumberString = "Dama";
			break;
		case 13:
			higherNumberString = "Rei";
			break;
		default:
			Debug.Log ("é numero normal");
			higherNumberString = higherNumberInt.ToString();
			break;
		}

		for (int i = 0; i < gc.allCenters.Count; i++) {

			if (gc.allCenters [i].transform.GetChild (0).name.Split (' ').Last () == "Copas") {
				totalPoints++;
			}

			if (gc.allCenters [i].transform.GetChild (0).name.Split(' ')[0].Contains(higherNumberString) && gc.allCenters [i].transform.GetChild (0).name.Split (' ').Last () == nipeOfTheRound) {
				print (gc.allCenters [i].transform.GetChild (0).name + " Ganhou o jogo");

				switch (gc.allCenters [i].name) {
				case "Center of Table 1":
					print ("Player1 é o dono da carta");
					DiscountPoints (1);
					break;
				case "Center of Table 2":
					print ("Player2 é o dono da carta");
					DiscountPoints (2);
					break;
				case "Center of Table 3":
					print ("Player3 é o dono da carta");
					DiscountPoints (3);
					break;
				case "Center of Table 4":
					print ("Player4 é o dono da carta");
					DiscountPoints (4);
					break;
				default:
					print ("Entrou default");
					break;
				}

			} else {
				print ("Deu erro");
			}
		}

		coroutine1 = StartCoroutine(ClearChild (gc.centerOFTable1, 1f));
		coroutine2 = StartCoroutine(ClearChild (gc.centerOFTable2, 1f));
		coroutine3 = StartCoroutine(ClearChild (gc.centerOFTable3, 1f));
		coroutine4 = StartCoroutine(ClearChild (gc.centerOFTable4, 1f));
	}

	IEnumerator ClearChild(GameObject parent, float delay){
		yield return new WaitForSeconds (delay);
		for (int i = 0; i < parent.transform.childCount; i++) {
			Destroy (parent.transform.GetChild (i).gameObject);
			yield return new WaitForFixedUpdate ();
		}
		if (gc.player1.transform.childCount < 1) {
			TheEnd ();
			yield return new WaitForSeconds(3);
			SceneManager.LoadScene ("New Prototype");
		}
		//gc.MakeTheMagicWithCards ();
		yield return null;
	}

	private void DiscountPoints(int player){

		totalPoints = 0;

		for (int i = 0; i < gc.allCenters.Count; i++) {
			if (gc.allCenters [i].transform.GetChild (0).name.Split (' ').Last() == "Copas") {
				totalPoints += 1;
				Debug.Log ("Tchrefows " + "perdeu " + totalPoints + " pontos");
			}

			if (gc.allCenters [i].transform.GetChild (0).name == "Dama de Espadas") {
				print ("Blooooooooooooooooow");
				totalPoints += 13;
			}
		}

		switch (player) {
		case 1:
			gc.points1 += totalPoints;
			p1Points.text = gc.points1.ToString ();
			break;
		case 2:
			gc.points2 += totalPoints;
			p2Points.text = gc.points2.ToString ();
			break;
		case 3:
			gc.points3 += totalPoints;
			p3Points.text = gc.points3.ToString ();
			break;
		case 4:
			gc.points4 += totalPoints;
			p4Points.text = gc.points4.ToString ();
			break;
		}

		totalPoints = 0;
	}

	private void TheEnd(){
		int minValue = Mathf.Min (gc.points1, gc.points2, gc.points3, gc.points4);
		int p1 = gc.points1;
		int p2 = gc.points2;
		int p3 = gc.points3;
		int p4 = gc.points4;

		if (minValue == p1) {
			whoWin.text = "Player 1 Win";
			whoWin.gameObject.SetActive (true);
		} else if (minValue == p2) {
			whoWin.text = "Player 2 Win";
			whoWin.gameObject.SetActive (true);
		} else if (minValue == p3) {
			whoWin.text = "Player 3 Win";
			whoWin.gameObject.SetActive (true);
		} else if (minValue == p4) {
			whoWin.text = "Player 4 Win";
			whoWin.gameObject.SetActive (true);
		}
	}

//	private string PlayerThatWon(){
//		
//	}
}
