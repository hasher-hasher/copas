﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class GameController : MonoBehaviour {

	//list prepared for receive a list of GameObjects that actually are the cards

	[SerializeField]
	public List<GameObject> cardsP1;
	public List<GameObject> cardsP2;
	public List<GameObject> cardsP3;
	public List<GameObject> cardsP4;

	//The 4 required players of the game

	public GameObject player1;
	public GameObject player2;
	public GameObject player3;
	public GameObject player4;

	//Variable that store player points
	public int points1 = 0;
	public int points2 = 0;
	public int points3 = 0;
	public int points4 = 0;

	//The variable able to receive the reference for the CardGenerator scripts

	//private CardGenerator cardgenerator;
	private NewGameController ngc;
	[SerializeField]
	public GameObject centerOFTable1;
	[SerializeField]
	public GameObject centerOFTable2;
	[SerializeField]
	public GameObject centerOFTable3;
	[SerializeField]
	public GameObject centerOFTable4;

	public List<GameObject> allCenters;

	private WorldRulez wr;

	private int whosTurn;

	private int returnVariableTryP;

	public void Start () {
		
		//cardgenerator = GameObject.FindGameObjectWithTag ("cardgenerator").GetComponent<CardGenerator>();
		ngc = GameObject.Find ("NewGameController").GetComponent<NewGameController>();
		wr = GameObject.Find ("WorldRulez").GetComponent<WorldRulez> ();

		player1 = GameObject.FindGameObjectWithTag ("player1");
		player2 = GameObject.FindGameObjectWithTag ("player2");
		player3 = GameObject.FindGameObjectWithTag ("player3");
		player4 = GameObject.FindGameObjectWithTag ("player4");

		allCenters = new List<GameObject> () {
			centerOFTable1,
			centerOFTable2,
			centerOFTable3,
			centerOFTable4
		};

		points1 = 0;
		points2 = 0;
		points3 = 0;
		points4 = 0;

		MakeTheMagicWithCards ();

		whosTurn = 1;

		ChangeChildrenComponent ();
	}

	public void Gaming(Transform card, string cardName){
		Destroy (card.GetComponent<EachCardAction>());
		switch (card.transform.parent.name) {
		case "Player 1":
			card.transform.position = centerOFTable1.transform.position;
			card.transform.SetParent (centerOFTable1.transform);
			card.tag = "card";
			break;
		case "Player 2":
			card.transform.position = centerOFTable2.transform.position;
			card.transform.SetParent (centerOFTable2.transform);
			card.tag = "card";
			break;
		case "Player 3":
			card.transform.position = centerOFTable3.transform.position;
			card.transform.SetParent (centerOFTable3.transform);
			card.tag = "card";
			break;
		case "Player 4":
			card.transform.position = centerOFTable4.transform.position;
			card.transform.SetParent (centerOFTable4.transform);
			card.tag = "card";
			break;
		default:
			Debug.Log ("Deu merda no Gaming function!!!");
			break;
		}

		if(whosTurn < 4){
			whosTurn++;
		}else{
			wr.WhoWinsAndCleanBoard();
			whosTurn = 1;
		}

		ChangeChildrenComponent ();

	}

	void ChangeChildrenComponent(){
		switch (whosTurn) {
		case 1:
			AddAllChildrenComponents (player1);
			RemoveAllChildrenComponents (player2);
			RemoveAllChildrenComponents (player3);
			RemoveAllChildrenComponents (player4);
			break;
		case 2:
			RemoveAllChildrenComponents (player1);
			AddAllChildrenComponents (player2);
			RemoveAllChildrenComponents (player3);
			RemoveAllChildrenComponents (player4);
			break;
		case 3:
			RemoveAllChildrenComponents (player1);
			RemoveAllChildrenComponents (player2);
			AddAllChildrenComponents (player3);
			RemoveAllChildrenComponents (player4);
			break;
		case 4:
			RemoveAllChildrenComponents (player1);
			RemoveAllChildrenComponents (player2);
			RemoveAllChildrenComponents (player3);
			AddAllChildrenComponents (player4);
			break;
		default:
			Debug.Log ("Ferrou tudo no ChangeChildrenComponents!!!");
			break;
		}
	}

	void AddAllChildrenComponents(GameObject parent){
		for (int i = 0; i < parent.transform.childCount; i++) {
			var bla = parent.transform.GetChild (i).gameObject.AddComponent<EachCardAction> () as EachCardAction;
		}
	}

	void RemoveAllChildrenComponents(GameObject parent){
		for (int i = 0; i < parent.transform.childCount; i++) {
			Destroy(parent.transform.GetChild (i).GetComponent<EachCardAction> ());
		}
	}

	public int SearchNumberInString(string stringToSearch){
		int theNumbers = int.Parse (Regex.Replace(stringToSearch, "[^0-9]", ""));
		return theNumbers;
	}

	public void MakeTheMagicWithCards(){
		//Always call this before "GenerateCardAndParentI"
		RandomCards.SplitValues ();

		//Populate the list with the random cards using the GenerateCard Function
		cardsP1 = ngc.GenerateCardAndParentIt (player1.transform);
		cardsP2 = ngc.GenerateCardAndParentIt (player2.transform);
		cardsP3 = ngc.GenerateCardAndParentIt (player3.transform);
		cardsP4 = ngc.GenerateCardAndParentIt (player4.transform);

		//whosTurn = 1;

	}
}
