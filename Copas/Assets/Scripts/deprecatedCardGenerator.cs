﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deprecatedCardGenerator : MonoBehaviour {

	//The reference for the blank GameObject

	[SerializeField]
	private GameObject rawCard;

	//Array with the respective image of the card

	public Sprite[] cardImgs;

	//Variables used for manipulate the numbers between string and int

	private string generateNumber;
	private string generateNipe;
	private string stringGenerateNipe;
	private string stringGenerateNumber;

	//temporary variable to instantiate all cards

	private GameObject tempCreatedGameobject;

	//Variable to store CardConfig script

	[SerializeField]
	private CardConfig cardConfig;

	private int arrayController = 0;

	string[,] storeRandom = new string[52, 52];

	//This function have the power to generate all cards, instantiate them and put into the right parent

	public List<GameObject> GenerateCard(GameObject parent){

		//Temporary list to populate the objects and them return all the cards generated in the end

		List<GameObject> gameObjectsToReturn = new List<GameObject>();

		//generate number and nipe in the organized function

		storeRandom = RandomCards.Randomize (13);

		//generate as many cards as mentioned as numberOfCards

		for (int i = 0; i < storeRandom.GetLength(0); i++) {

			//generate random numbers for card number and card nipe

			generateNumber = storeRandom [i, 0];
			generateNipe = storeRandom [i, 1];

			stringGenerateNumber = storeRandom [i, 0];

			//generateNumber = Random.Range (1, 13);
			//generateNipe = Random.Range (1, 4);

			//stringGenerateNumber = generateNumber.ToString ();

			switch (generateNipe) {
			case "Paus":
				stringGenerateNipe = "P";	
				break;
			case "Ouro":
				stringGenerateNipe = "O";	
				break;
			case "Copas":
				stringGenerateNipe = "C";	
				break;
			case "Espadas":
				stringGenerateNipe = "E";	
				break;
			default:
				Debug.Log ("Deu merda na verificacao no nipe!!!");
				break;
			}

			//If first object instantiated, instantiate at the extreme left position, and the next will be created 0.6f at the right of the previous card

			if (gameObjectsToReturn.Count > 0) {
				tempCreatedGameobject = Instantiate (rawCard, (gameObjectsToReturn[gameObjectsToReturn.Count - 1].transform.localPosition + new Vector3(0.6f, 0f, gameObjectsToReturn.Count)), Quaternion.identity);
			} else {
				tempCreatedGameobject = Instantiate (rawCard, Vector3.zero, Quaternion.identity);
			}

			//put the right sprite in GameObject

			tempCreatedGameobject.GetComponent<SpriteRenderer> ().sprite = SearchSprite (stringGenerateNumber, stringGenerateNipe);

			//Setting the right parent to the GameObject

			tempCreatedGameobject.transform.SetParent (parent.transform, false);

			//Setting the right card name for the GameObject

			tempCreatedGameobject.name = cardConfig.allCards [ArrayMath (stringGenerateNumber, stringGenerateNipe), 0] + " de " + cardConfig.allCards [ArrayMath (stringGenerateNumber, stringGenerateNipe), 1];

			//Populate the gameObjectsToReturn list with the generated GameObject

			gameObjectsToReturn.Add (tempCreatedGameobject);
		}

		//Running through gameObjectsToReturn list applying the right position relative to the parent

		foreach (GameObject gObj in gameObjectsToReturn) {
			arrayController++;
			gObj.transform.localPosition = new Vector3 (gObj.transform.localPosition.x - (0.6f * (gameObjectsToReturn.Count/2)), 0f, (-0.1f * (float)arrayController));
		}

		//Return the list of GameObjects created

		return gameObjectsToReturn;
	}

	//This function return the right sprite in the cardImgs array

	public Sprite SearchSprite(string number, string nipe){

		return cardImgs [ArrayMath (number, nipe)];
	}

	//This function do the magic! It returns the right position in the array related to the number and the name of the card

	public int ArrayMath(string number, string nipe){
		int internNumber = 0;

		switch (number) {
		case "A":
			//Debug.Log ("Gerou A");
			internNumber = 1 - 1;
			break;
		case "Valete":
			Debug.Log ("Gerou Valete");
			internNumber = 10 - 1;
			break;
		case "Dama":
			Debug.Log ("Gerou Dama");
			internNumber = 11 - 1;
			break;
		case "Rei":
			Debug.Log ("Gerou Rei");
			internNumber = 12 - 1;
			break;
		default:
			Debug.Log ("Deu Ruim");
			Debug.Log (number);
			break;
		}

		internNumber = int.Parse (number) - 1;
		int internNipe = 0;

		switch (nipe) {
		case "P":
			internNipe = 0;
			break;
		case "O":
			internNipe = 1;
			break;
		case "C":
			internNipe = 2;
			break;
		case "E":
			internNipe = 3;
			break;
		default:
			Debug.Log("Problema no retorno da carta correta!!!");
			break;
		}

		return ((internNumber * 4) + internNipe);
	}
}
