﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardConfig : MonoBehaviour {

	//A card name 2D array created for return the name of each card

	public string[,] allCards = {
		{"A","Paus"},
		{"A","Ouro"},
		{"A","Copas"},
		{"A","Espadas"},
		{"2","Paus"},
		{"2","Ouro"},
		{"2","Copas"},
		{"2","Espadas"},
		{"3","Paus"},
		{"3","Ouro"},
		{"3","Copas"},
		{"3","Espadas"},
		{"4","Paus"},
		{"4","Ouro"},
		{"4","Copas"},
		{"4","Espadas"},
		{"5","Paus"},
		{"5","Ouro"},
		{"5","Copas"},
		{"5","Espadas"},
		{"6","Paus"},
		{"6","Ouro"},
		{"6","Copas"},
		{"6","Espadas"},
		{"7","Paus"},
		{"7","Ouro"},
		{"7","Copas"},
		{"7","Espadas"},
		{"8","Paus"},
		{"8","Ouro"},
		{"8","Copas"},
		{"8","Espadas"},
		{"9","Paus"},
		{"9","Ouro"},
		{"9","Copas"},
		{"9","Espadas"},
		{"10","Paus"},
		{"10","Ouro"},
		{"10","Copas"},
		{"10","Espadas"},
		{"Valete","Paus"},
		{"Valete","Ouro"},
		{"Valete","Copas"},
		{"Valete","Espadas"},
		{"Dama","Paus"},
		{"Dama","Ouro"},
		{"Dama","Copas"},
		{"Dama","Espadas"},
		{"Rei","Paus"},
		{"Rei","Ouro"},
		{"Rei","Copas"},
		{"Rei","Espadas"},
	};
}