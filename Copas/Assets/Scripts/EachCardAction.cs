﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EachCardAction : MonoBehaviour {

	private Vector3 originPosition;
	private Vector3 selectionPosition;

	private bool selected;

	private GameController gameController;

	private WorldRulez wr;

	void Start(){
		selected = false;
		gameController = GameObject.Find ("GameController").GetComponent<GameController>();
		wr = GameObject.Find ("WorldRulez").GetComponent<WorldRulez> ();
	}

	void OnMouseEnter() {
		originPosition = transform.localPosition;
		if (!selected) {
			transform.localPosition = new Vector3 (originPosition.x, originPosition.y + 0.1f, originPosition.z);
			selected = !selected;
		}
		//print (gameObject.name);
	}

	void OnMouseExit() {
		if (selected) {
			transform.localPosition = new Vector3 (originPosition.x, originPosition.y, originPosition.z);
			selected = !selected;
		}
	}

	void OnMouseDown() {
		//Check if the card can be played or not
		if (wr.CanIPlayThisCard (gameObject.name, gameObject)) {
			gameController.Gaming (gameObject.transform, gameObject.name);
		} else {
			Debug.Log ("Não pode jogar esta carta");
		}
	}
}
