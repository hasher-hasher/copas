﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomCards {

	//CardConfig instance
	private static string[,] allCards = new string[,]{
		{"A","Paus"},
		{"A","Ouro"},
		{"A","Copas"},
		{"A","Espadas"},
		{"2","Paus"},
		{"2","Ouro"},
		{"2","Copas"},
		{"2","Espadas"},
		{"3","Paus"},
		{"3","Ouro"},
		{"3","Copas"},
		{"3","Espadas"},
		{"4","Paus"},
		{"4","Ouro"},
		{"4","Copas"},
		{"4","Espadas"},
		{"5","Paus"},
		{"5","Ouro"},
		{"5","Copas"},
		{"5","Espadas"},
		{"6","Paus"},
		{"6","Ouro"},
		{"6","Copas"},
		{"6","Espadas"},
		{"7","Paus"},
		{"7","Ouro"},
		{"7","Copas"},
		{"7","Espadas"},
		{"8","Paus"},
		{"8","Ouro"},
		{"8","Copas"},
		{"8","Espadas"},
		{"9","Paus"},
		{"9","Ouro"},
		{"9","Copas"},
		{"9","Espadas"},
		{"10","Paus"},
		{"10","Ouro"},
		{"10","Copas"},
		{"10","Espadas"},
		{"Valete","Paus"},
		{"Valete","Ouro"},
		{"Valete","Copas"},
		{"Valete","Espadas"},
		{"Dama","Paus"},
		{"Dama","Ouro"},
		{"Dama","Copas"},
		{"Dama","Espadas"},
		{"Rei","Paus"},
		{"Rei","Ouro"},
		{"Rei","Copas"},
		{"Rei","Espadas"},
	};

	//Array with the copy of the CardConfig instance
	private static string[,] cardsArray = new string[53,53];

	//Lists to store the number and the nipe of the cards after treatment
	private static List<string> numberList = new List<string> ();
	private static List<string> nipeList = new List<string> ();

	public static void SplitValues(){
		for (int i = 0; i < allCards.GetLength (0); i++) {
			numberList.Add(allCards [i, 0]);
			nipeList.Add(allCards [i, 1]);
		}
	}

	public static string[,] Randomize(int cardsToGenerate){
		string[,] response = new string[cardsToGenerate, cardsToGenerate];
		int randomNumber = 0;

		for(int i=0;i < cardsToGenerate;i++){
			randomNumber = Random.Range (0, numberList.Count);
			response [i, 0] = numberList [randomNumber];
			response [i, 1] = nipeList [randomNumber];

			GarbageCollector (randomNumber);
		}

		return response;
	}

	//Remove a card the was drawed once
	private static void GarbageCollector(int indexToClean){
		numberList.RemoveAt (indexToClean);
		nipeList.RemoveAt (indexToClean);
	}
}
